from django.test import TestCase
from django.test import Client
from .models import Status
from .views import display
from selenium import webdriver

# Create your tests here.

class DataTest(TestCase):
	
	# initialize test
	def setUp(self):
		self.browser = webdriver.Firefox()
		self.client = Client()
		self.server = self.client.get("")

	# checks if server exists
	def testServerExists(self):
		self.assertEqual(self.server.status_code, 200)

	# check if the correct page is received
	def testPageExists(self):
		self.assertTemplateUsed(self.server, "status.html")
	
	# check if the form exists		
	def testFormExists(self):
		self.formConf = self.browser.find_element_by_id("massage")
		self.submitConf = self.browser.find_element_by_name("submit-button")
		self.assertIsNotNone(self.formConf)
		self.assertIsNotNone(self.submitConf)
	
	# check if status exists in database
	def testStatusExists(self):
		status = Status.objects.create(massage = "Direct model input test")
		status.save()
		
		self.assertEqual(1, Status.objects.all().count())
	
	# check if the form can be used
	def testFormAbleToPost(self):
		self.formConf.send_keys("Post input test")
		self.submitConf.click()
		
		self.assertEqual(2, Status.objects.all().count())
	
	# check if status is displayed on screen
	def testStatusDisplayed(self):
		displayedStatus = self.server.content.decode("utf8")
		self.assertIn("Post input test", displayedStatus)
		self.assertIn("Direct model input test", displayedStatus)
	
	# end the test
	def tearDown(self):
		self.browser.quit()
		
