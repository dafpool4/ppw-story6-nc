from django.shortcuts import render
from .models import Status
from .forms import StatusForm

# Create your views here.

def display(request):
    form = StatusForm(request.POST)
    manager = Status.objects
    
    if form.is_valid():
    	massageIn = form.cleaned_data["massage"]
    	
    	stat = manager.create(massage = massageIn)
    	stat.save()
    
    response = {
    	"status_form" : form,
    	"status_display" : display,
    }
    
    return render(request, "status.html", response)
